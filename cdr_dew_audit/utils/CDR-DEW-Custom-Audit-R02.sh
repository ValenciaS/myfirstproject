#!/bin/bash

#########################################################################
#-- Name : CDR-DEW-Custom-Audit-R02.sh
#-- Description : Use this script in Hue to run python code. 
#--
#-- Created By and on : Hardik Patira and 27-Jun-2019
#-- Edit History
#-- Hardik Patira -- Dec 2, 2019 -- Changes due to re-structuring the folders/files due to devops 
######################################################################### 
 

echo "Starting the script"
function run() {
 echo "Running Python code"
 rm -rf /opt/CDR/DEW/python/cdr_dew_audit/data/audit.csv
 cd /opt/CDR/DEW/python/cdr_dew_audit
 /opt/CDR/anaconda3/envs/cdr_dew_audit_venv/bin/python main.py
 chmod 775 /opt/CDR/DEW/python/cdr_dew_audit/data/audit.csv 
 chgrp trifactausers /opt/CDR/DEW/python/cdr_dew_audit/data/audit.csv
 echo "Python Script completed"
}; 

run; 

echo "Moving CSV file from local to Hadoop"
hadoop fs -copyFromLocal /opt/CDR/DEW/python/cdr_dew_audit/data/audit.csv /data/CDR/audit/trifacta
hadoop fs -setfacl -m group:trifactausers:rwx /data/CDR/audit/trifacta/audit.csv
echo "File moving commnds done here"

echo "Script Completed"
