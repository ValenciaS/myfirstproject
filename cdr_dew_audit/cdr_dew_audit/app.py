from cdr_dew_audit import audit
import configparser as cp


def run():

    props = cp.RawConfigParser()
    props.read("resources/application.properties")
    env = 'env'

    username = props.get(env, 'credential.username' )
    password = props.get(env, 'credential.password')
    url = props.get(env, 'trifacta.url')
    filename = props.get(env,'output.filename')

    flows = audit.get_flow_audit(url, username, password)
    recipes = audit.get_recipe_audit(url, username, password)
    peoples = audit.get_people_audit(url, username, password)


    result = audit.audit_result(flows, recipes, peoples)

    result = result[['flow_id',
                     'flow_name',
                     'flow_description',
                     'flow_created_by',
                     'flow_created_by_email',
                     'flow_created_timestamp',
                     'flow_updated_by',
                     'flow_updated_by_email',
                     'flow_updated_timestamp',
                     'recipe_id',
                     'recipe_name',
                     'recipe_description',
                     'recipe_created_by',
                     'recipe_created_by_email',
                     'recipe_created_timestamp',
                     'recipe_updated_by',
                     'recipe_updated_by_email',
                     'recipe_updated_timestamp']]

    standard_column_name = {'flow_id': 'Flow ID',
                            'flow_name': 'Flow name',
                            'flow_description': 'Flow Description',
                            'flow_created_by': 'Flow created by',
                            'flow_created_by_email': 'Flow created by email',
                            'flow_created_timestamp': 'Flow created timestamp',
                            'flow_updated_by': 'Flow updated by',
                            'flow_updated_by_email': 'Flow updated by email',
                            'flow_updated_timestamp': 'Flow updated timestamp',
                            'recipe_id': 'Recipe ID',
                            'recipe_name': 'Recipe name',
                            'recipe_description': 'Recipe description',
                            'recipe_created_by': 'Recipe created by',
                            'recipe_created_by_email': 'recipe created by email',
                            'recipe_created_timestamp': 'Recipe created timestamp',
                            'recipe_updated_by': 'Recipe updated by',
                            'recipe_updated_by_email': 'Recipe updated by email',
                            'recipe_updated_timestamp': 'Recipe updated timestamp'}

    result = result.rename(columns=standard_column_name)

    result.to_csv(filename, index=False, encoding='utf-8-sig')




