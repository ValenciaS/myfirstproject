import requests
from requests.auth import HTTPBasicAuth
import json
import pandas as pd


def get_flow_audit(url, username, password):

    my_response = requests.get(url + 'flows?limit=999999999', auth=HTTPBasicAuth(username, password), verify=False)

    if my_response.ok:
        j_data = json.loads(my_response.content)
        flow_raw_data = pd.DataFrame(j_data)
        flow_columns = {'id': 'flow_id',
                        'name': 'flow_name',
                        'description': 'flow_description',
                        'createdAt': 'flow_created_timestamp',
                        'updatedAt': 'flow_updated_timestamp',
                        'createdBy': 'flow_created_id',
                        'updatedBy': 'flow_updated_id'}
        flow_data = flow_raw_data[['id', 'name', 'description', 'createdAt', 'updatedAt', 'createdBy', 'updatedBy']]
        flow_data = flow_data.rename(columns=flow_columns)
        return flow_data

    else:
        # If response code is not ok (200), print the resulting http error code with description
        my_response.raise_for_status()


def get_recipe_audit(url, username, password):

    my_response = requests.get(url + 'wrangledDatasets?limit=999999999', auth=HTTPBasicAuth(username, password), verify=False)

    if my_response.ok:
        j_data = json.loads(my_response.content)
        recipe_raw_data = pd.DataFrame(j_data['data'])
        recipe_data = recipe_raw_data[['id', 'name', 'description', 'createdAt', 'updatedAt', 'createdBy', 'updatedBy']]
        recipe_id = recipe_raw_data['flow'].apply(pd.Series).rename(columns={'id': 'recipe_flow_id'})
        recipe_data = pd.concat([recipe_data, recipe_id], axis=1)
        recipe_columns = {'id': 'recipe_id',
                        'name': 'recipe_name',
                        'description': 'recipe_description',
                        'createdAt': 'recipe_created_timestamp',
                        'updatedAt': 'recipe_updated_timestamp',
                        'createdBy': 'recipe_created_id',
                        'updatedBy': 'recipe_updated_id'}
        recipe_data = pd.DataFrame(recipe_data).rename(columns=recipe_columns)
        return recipe_data
        # return flow_data

    else:
        # If response code is not ok (200), print the resulting http error code with description
        my_response.raise_for_status()


def get_people_audit(url, username, password):
    my_response = requests.get(url + 'people?limit=999999999', auth=HTTPBasicAuth(username, password), verify=False)

    if my_response.ok:
        j_data = json.loads(my_response.content)
        people_raw_data = pd.DataFrame(j_data['data'])
        people_data = people_raw_data[['id', 'name', 'email']]
        return people_data
        # return flow_data

    else:
        # If response code is not ok (200), print the resulting http error code with description
        my_response.raise_for_status()


def audit_result(flows, recipes, peoples):
    flow_recipes_merge = pd.merge(flows, recipes, how='inner', left_on='flow_id', right_on='recipe_flow_id')
    flow_recipes_merge = pd.merge(flow_recipes_merge, peoples, how='inner', left_on='flow_created_id', right_on='id') \
        .rename(columns={'id': 'flow_created_id', 'name': 'flow_created_by', 'email': 'flow_created_by_email'})
    flow_recipes_merge = pd.merge(flow_recipes_merge, peoples, how='inner', left_on='flow_updated_id', right_on='id') \
        .rename(columns={'id': 'flow_updated_id', 'name': 'flow_updated_by', 'email': 'flow_updated_by_email'})
    flow_recipes_merge = pd.merge(flow_recipes_merge, peoples, how='inner', left_on='recipe_created_id', right_on='id') \
        .rename(columns={'id': 'recipe_created_id', 'name': 'recipe_created_by', 'email': 'recipe_created_by_email'})
    flow_recipes_merge = pd.merge(flow_recipes_merge, peoples, how='inner', left_on='recipe_updated_id', right_on='id') \
        .rename(columns={'id': 'recipe_updated_id', 'name': 'recipe_updated_by', 'email': 'recipe_updated_by_email'})

    return flow_recipes_merge
