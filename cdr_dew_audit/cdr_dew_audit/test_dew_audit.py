from cdr_dew_audit import audit
import pytest
import json

flow_audit = None
recipe_audit = None
people_audit = None

mocked_trifacta_url = "http://localhost:5000/"
mocked_trifacta_user = "ycdrtd"
mocked_trifacta_pass = "4@eVBz9c"

flow = '''[
        {
        "id": 2175,
        "name": "DEV_201791_DATACLEAN_AE003_RECON",
        "description": "Recon flow updated as part of DEW Release 3.2 (Du Toit,Chris)",
        "deleted_at": null,
        "cpProject": null,
        "workspaceId": 1,
        "folderId": 96,
        "createdAt": "2020-11-11T07:28:22.674Z",
        "updatedAt": "2020-11-11T07:34:48.389Z",
        "createdBy": 18,
        "updatedBy": 18
    }]'''
recipe = '''{"data": [
        {
            "id": 25312,
            "wrangled": true,
            "referenceId": null,
            "createdAt": "2020-11-11T07:28:22.700Z",
            "updatedAt": "2020-11-11T07:28:22.700Z",
            "createdBy": 18,
            "updatedBy": 18,
            "name": "HISTORICAL",
            "description": null,
            "activeSample": null,
            "flow": {
                "id": 2175
            },
            "script": {
                "id": 20098
            }
        }]}'''
people = '''{"data": [
        {
            "id": 18,
            "email": "chris.dutoit@quintiles.com",
            "name": "q728961"
        },
        {
            "id": 33,
            "email": "ycdrtd@quintiles.net",
            "name": "ycdrtd",
            "ssoPrincipal": "ycdrtd",
            "hadoopPrincipal": "ycdrtd",
            "cpPrincipal": null,
            "isAdmin": false,
            "isDisabled": false,
            "forcePasswordChange": false,
            "state": "active",
            "lastStateChange": null,
            "createdAt": "2019-04-17T10:37:01.320Z",
            "updatedAt": "2020-05-27T11:40:46.623Z",
            "outputHomeDir": "/trifacta/queryResults/ycdrtd@quintiles.net",
            "fileUploadPath": "/trifacta/uploads",
            "awsConfig": null
        }]}'''


@pytest.mark.server(url='/flows', response=json.loads(flow), method='GET')
def test_get_flow_audit():
    global flow_audit
    flow_audit = audit.get_flow_audit(mocked_trifacta_url, mocked_trifacta_user, mocked_trifacta_pass)
    assert flow_audit.iloc[0]['flow_id'] == 2175


    
@pytest.mark.server(url='/wrangledDatasets', response=json.loads(recipe), method='GET')
def test_get_recipe_audit():
    global recipe_audit
    recipe_audit = audit.get_recipe_audit(mocked_trifacta_url, mocked_trifacta_user, mocked_trifacta_pass)
    assert recipe_audit.iloc[0]['recipe_id'] == 25312


@pytest.mark.server(url='/people', response=json.loads(people), method='GET')
def test_get_people_audit():
    global people_audit
    people_audit = audit.get_people_audit(mocked_trifacta_url, mocked_trifacta_user, mocked_trifacta_pass)
    assert people_audit.iloc[0]['id'] == 18
    
def test_audit_result():
    global flow_audit,recipe_audit,people_audit
    result_audit = audit.audit_result(flow_audit,recipe_audit,people_audit)
    assert result_audit.iloc[0]['flow_id'] == 2175
    assert result_audit.iloc[0]['recipe_id'] == 25312
    assert result_audit.iloc[0]['flow_created_by'] == 'q728961'
    assert len(result_audit.columns) == 27

    
